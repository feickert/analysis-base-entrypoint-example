ARG BASE_IMAGE=gitlab-registry.cern.ch/atlas/athena/analysisbase:22.2.113
FROM ${BASE_IMAGE} as base

USER root

COPY --chown=atlas entrypoint.sh /entrypoint.sh

USER atlas

# Ensure any login shell and any interactive sets up release
RUN printf '\n# Setup release\nif [ -f /release_setup.sh ]; then\n    echo "# . /release_setup.sh"\n    . /release_setup.sh\nfi\n' >> /home/atlas/.bashrc

ENTRYPOINT [ "/entrypoint.sh" ]
CMD [ "/bin/bash" ]
