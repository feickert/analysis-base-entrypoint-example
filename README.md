# analysis-base-entrypoint-example

This repository demonstrates a minimal extension to an ATLAS AnalysisBase Docker image

```console
$ wc --lines --words Dockerfile entrypoint.sh
 14  51 Dockerfile
  4  15 entrypoint.sh
 18  66 total
```

such that the desired analysis environment provided by `/release_setup.sh` in the AnalysisBase image is automatically setup at runtime.

## Why does this work?

The general approach is to try to ensure that login shell behavior is always invoked, as this ensures that `~/.bash_profile` will get sourced which will then source `~/.bashrc`.

From the `bash` manual:

>    INVOCATION
>
>    A login shell is one whose first character of argument zero is a -, or one started with the --login option.
>
>    ...
>
>    When bash is invoked as an interactive login shell, or as a non-interactive shell with the --login option, it first reads and executes commands from  the  file  /etc/profile,  if that  file  exists.
>    After reading that file, it looks for ~/.bash_profile, ~/.bash_login, and ~/.profile, in that order, and reads and executes commands from the first one that exists and is readable.

With this in mind, we place the command that we want to always run (`. /release_setup.sh`) in `~/.bashrc` and then have `ENTRYPOINT` execute all arguments passed to as if it was a login shell.

## Runtime examples

First pull the image from the GitLab container registry

```
docker pull gitlab-registry.cern.ch/feickert/analysis-base-entrypoint-example:22.2.113
```

### Default interactive shell

```console
$ docker run --rm -ti gitlab-registry.cern.ch/feickert/analysis-base-entrypoint-example:22.2.113
# . /release_setup.sh
Configured GCC from: /opt/lcg/gcc/11.2.0-8a51a/x86_64-centos7/bin/gcc
Configured AnalysisBase from: /usr/AnalysisBase/22.2.113/InstallArea/x86_64-centos7-gcc11-opt
[bash][atlas AnalysisBase-22.2.113]:workdir > command -v root
/usr/AnalysisBaseExternals/22.2.113/InstallArea/x86_64-centos7-gcc11-opt/bin/root
[bash][atlas AnalysisBase-22.2.113]:workdir > command -v python
/usr/AnalysisBaseExternals/22.2.113/InstallArea/x86_64-centos7-gcc11-opt/bin/python
[bash][atlas AnalysisBase-22.2.113]:workdir > python --version --version
Python 3.9.12 (main, Mar 16 2023, 02:25:00)
[GCC 11.2.0]
[bash][atlas AnalysisBase-22.2.113]:workdir >
```

### User provided commands

```console
$ docker run --rm gitlab-registry.cern.ch/feickert/analysis-base-entrypoint-example:22.2.113 /bin/bash -c 'command -v root && command -v python && python --version --version'
# . /release_setup.sh
Configured GCC from: /opt/lcg/gcc/11.2.0-8a51a/x86_64-centos7/bin/gcc
Configured AnalysisBase from: /usr/AnalysisBase/22.2.113/InstallArea/x86_64-centos7-gcc11-opt
/usr/AnalysisBaseExternals/22.2.113/InstallArea/x86_64-centos7-gcc11-opt/bin/root
/usr/AnalysisBaseExternals/22.2.113/InstallArea/x86_64-centos7-gcc11-opt/bin/python
Python 3.9.12 (main, Mar 16 2023, 02:25:00)
[GCC 11.2.0]
```

### User overrides entrypoint and gets interactive shell

As the shell is interactive the environment is still setup

```console
$ docker run --rm -ti --entrypoint /bin/bash gitlab-registry.cern.ch/feickert/analysis-base-entrypoint-example:22.2.113
# . /release_setup.sh
Configured GCC from: /opt/lcg/gcc/11.2.0-8a51a/x86_64-centos7/bin/gcc
Configured AnalysisBase from: /usr/AnalysisBase/22.2.113/InstallArea/x86_64-centos7-gcc11-opt
[bash][atlas AnalysisBase-22.2.113]:workdir > command -v root
/usr/AnalysisBaseExternals/22.2.113/InstallArea/x86_64-centos7-gcc11-opt/bin/root
[bash][atlas AnalysisBase-22.2.113]:workdir >
```

### User overrides entrypoint and provides commands

As the entrypoint has been overridden and the shell is non-interactive then the user needs to manually setup the environment

```console
$ docker run --rm -ti --entrypoint /bin/bash gitlab-registry.cern.ch/feickert/analysis-base-entrypoint-example:22.2.113 -c '. /release_setup.sh && command -v root && command -v python && python --version --version'
Configured GCC from: /opt/lcg/gcc/11.2.0-8a51a/x86_64-centos7/bin/gcc
Configured AnalysisBase from: /usr/AnalysisBase/22.2.113/InstallArea/x86_64-centos7-gcc11-opt
/usr/AnalysisBaseExternals/22.2.113/InstallArea/x86_64-centos7-gcc11-opt/bin/root
/usr/AnalysisBaseExternals/22.2.113/InstallArea/x86_64-centos7-gcc11-opt/bin/python
Python 3.9.12 (main, Mar 16 2023, 02:25:00)
[GCC 11.2.0]
```

unless the user makes the entrypoint shell a login shell

```console
$ docker run --rm -ti --entrypoint /bin/bash gitlab-registry.cern.ch/feickert/analysis-base-entrypoint-example:22.2.113 -l -c 'command -v root && command -v python && python --version --version'
# . /release_setup.sh
Configured GCC from: /opt/lcg/gcc/11.2.0-8a51a/x86_64-centos7/bin/gcc
Configured AnalysisBase from: /usr/AnalysisBase/22.2.113/InstallArea/x86_64-centos7-gcc11-opt
/usr/AnalysisBaseExternals/22.2.113/InstallArea/x86_64-centos7-gcc11-opt/bin/root
/usr/AnalysisBaseExternals/22.2.113/InstallArea/x86_64-centos7-gcc11-opt/bin/python
Python 3.9.12 (main, Mar 16 2023, 02:25:00)
[GCC 11.2.0]
```
