build:
	docker build \
		--file Dockerfile \
		--build-arg BASE_IMAGE=gitlab-registry.cern.ch/atlas/athena/analysisbase:22.2.113 \
		--tag gitlab-registry.cern.ch/feickert/analysis-base-entrypoint-example:22.2.113 \
		.
	docker system prune -f

run:
	docker run \
		--rm \
		-ti \
		--user 1000:1000 \
		gitlab-registry.cern.ch/feickert/analysis-base-entrypoint-example:22.2.113
